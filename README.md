# c0sv

This is a binary CSV format.  Incredibly simple, and separated by ASCII Control
characters.  This uses SOH (Start of Heading), STX (Start of Text), ETX (End of
Text), ESC (Escape), US (Unit Separator), and RS (Record Separator).

The stream is expressed in the following faux-EBNF (where * represents any
single byte):

```ebnf
stream = { message }, [EOT]
message = [header], STX, records
header = SOH, {units}
records = { RS, units }
units = { US, unit }
unit = { (* - control) | (ESC, *) }
control = SOH | STX | ESC | US | RS | EOT
```

This is mostly a simple experiment to see how feasible it would be to create a
very CSV-like format using these ASCII control characters for delimitation (and
particularly to use the control characters in the way they are intended to be
used).  It's probably not extraordinarily useful, because the only real purpose
of CSV is exchange where manual readability and/or writability is important.  If
you want good binary flexibility, you're probably better off using a good binary
format, like bincode or messagepack.

Still, this does have some convenient aspects, such as the fact that it is
rather easily streamable, allowing processing records while needing only one in
memory at a time.  At the expense of being slower to parse, this format is
capable of being slightly smaller than most other binary formats, as there are
no length prefixes.

# Advantages

Over CSV:

* Headers are explicitly delimited, so there is never any guessing about whether
  the first row constitutes a header.
* There is only one method of escaping, so there is not any need for confusion
  about how to handle special characters like commas or newlines in fields.
  Parsing is also simpler in this case, as CSV parsers often try naively to
  process interleaved newlines.
* Multiple documents with headers can be concatenated in the same stream and
  parsed without any loss.
* The optional EOT to end the stream allows a c0sv to be embedded in the middle
  of more data and parsed without issues using just a pointer to its start byte.

# Disadvantages

* Control characters aren't really printable, so this format can not be easily
  edited with a text editor.
* Because strings are not length-prefixed, parsing strings has to be done
  character-by-character, and escape sequences can interfere with no-copy
  reading.
